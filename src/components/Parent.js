import { Component } from "react";
import Child from "./Child";

class Parent extends Component {
    constructor(props) {
        super(props);
        this.state = {
            ball1: null,
            ball2: null,
            ball3: null,
            ball4: null,
            ball5: null,
            ball6: null
        }
    }
    generate = () => {
        this.setState({
            ball1: Math.floor(Math.random() * 99 + 1),
            ball2: Math.floor(Math.random() * 99 + 1),
            ball3: Math.floor(Math.random() * 99 + 1),
            ball4: Math.floor(Math.random() * 99 + 1),
            ball5: Math.floor(Math.random() * 99 + 1),
            ball6: Math.floor(Math.random() * 99 + 1)
        })
    }
    render(){
        return(
            <div>
                <Child generate = {this.generate}
                    ball1 = {this.state.ball1}
                    ball2 = {this.state.ball2}
                    ball3 = {this.state.ball3}
                    ball4 = {this.state.ball4}
                    ball5 = {this.state.ball5}
                    ball6 = {this.state.ball6}
                />
            </div>
        )
    }
}

export default Parent