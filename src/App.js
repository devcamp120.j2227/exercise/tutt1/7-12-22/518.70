import './App.css';
import Parent from './components/Parent';
import "bootstrap/dist/css/bootstrap.min.css";

function App() {
  return (
    <div className="App">
      <Parent/>
    </div>
  );
}

export default App;
